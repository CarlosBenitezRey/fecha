package com.example.fecha

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import java.util.Calendar

private lateinit var etDate: EditText
private lateinit var button2: Button
private lateinit var textView3: TextView

class MainActivity : AppCompatActivity() {
    var sumatotal=0
    var total=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var meseleccionado: Int
        var anioseleccionado: Int
        val calendario= Calendar.getInstance()
        var diaselecionado= Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        meseleccionado= calendario.get(Calendar.MONTH)
        //meseleccionado+=1
        anioseleccionado= calendario.get(Calendar.YEAR)
        var dayinmin= diaselecionado*1440
        var mesinmin= meseleccionado*43800
        var yearinmin= anioseleccionado*525600
        sumatotal= dayinmin+mesinmin+yearinmin
        etDate= findViewById(R.id.etDate)
        textView3=findViewById(R.id.textView3)
        button2=findViewById(R.id.button2)
        etDate.setOnClickListener { showDatePickerDialog() }
        button2.setOnClickListener{
            textView3.text= "La fecha de hoy es: $diaselecionado/$meseleccionado/$anioseleccionado, " +
                    "la fecha que ingresaste, hasta el dia de hoy en minutos es:$total"
        }
    }

    fun showDatePickerDialog() {
        val datePicker = DatePickerFragment { day, month, year -> onDateSelected(day, month, year) }
        datePicker.show(supportFragmentManager, "datePicker")
    }
     private fun onDateSelected(day: Int, month: Int, year: Int) {
         val messs= month+1
        etDate.setText("$day/$messs/$year")
         var sumado2= (day*1440)+((month)*43800)+(year*525600)
         total= sumatotal - sumado2
         //textView3.text= "la fecha que ingresaste, hasta el dia de hoy en minutos es:$total"

    }
}